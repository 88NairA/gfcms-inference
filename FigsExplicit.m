figure  
for a1=1:50
point1c1=[final{a1,1}{1,1}(1,1) a1 0];
point2c1=[final{a1,1}{1,1}(1,2) a1 1];
point3c1=[final{a1,1}{1,1}(1,3) a1 0];
pointsc1=[point1c1' point2c1' point3c1'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc1(1,:),pointsc1(2,:),pointsc1(3,:),'b'), hold on
scatter3(LstCenC1{a1,1}{1,2}, a1, LstCenC1{a1,1}{1,3}, '*'), hold on
grid on
alpha(0);
end
 

%plotting triangles and centroids for Concept 2
figure
for a2=1:50
point1c2=[final{a2,1}{1,2}(1,1) a2 0];
point2c2=[final{a2,1}{1,2}(1,2) a2 1];
point3c2=[final{a2,1}{1,2}(1,3) a2 0];
pointsc2=[point1c2' point2c2' point3c2'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc2(1,:),pointsc2(2,:),pointsc2(3,:),'b'), hold on
scatter3(LstCenC2{a2,1}{1,2}, a2, LstCenC2{a2,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

 
%plotting triangles and centroids for Concept 3
 
figure
for a3=1:50
point1c3=[final{a3,1}{1,3}(1,1) a3 0];
point2c3=[final{a3,1}{1,3}(1,2) a3 1];
point3c3=[final{a3,1}{1,3}(1,3) a3 0];
pointsc3=[point1c3' point2c3' point3c3'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc3(1,:),pointsc3(2,:),pointsc3(3,:),'b'), hold on
scatter3(LstCenC3{a3,1}{1,2}, a3, LstCenC3{a3,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

 
 
%plotting triangles and centroids for Concept 4 for 21 iterations
figure
 
for a4=1:50
point1c4=[final{a4,1}{1,4}(1,1) a4 0];
point2c4=[final{a4,1}{1,4}(1,2) a4 1];
point3c4=[final{a4,1}{1,4}(1,3) a4 0];
pointsc4=[point1c4' point2c4' point3c4'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]); 
fill3(pointsc4(1,:),pointsc4(2,:),pointsc4(3,:),'b'), hold on
scatter3(LstCenC4{a4,1}{1,2}, a4, LstCenC4{a4,1}{1,3}, '*'), hold on
grid on
alpha(0);
end
 
 
%plotting triangles and centroids for Concept 5
figure
for a5=1:50
point1c5=[final{a5,1}{1,5}(1,1) a5 0];
point2c5=[final{a5,1}{1,5}(1,2) a5 1];
point3c5=[final{a5,1}{1,5}(1,3) a5 0];
pointsc5=[point1c5' point2c5' point3c5'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]); 
fill3(pointsc5(1,:),pointsc5(2,:),pointsc5(3,:),'b'), hold on
scatter3(LstCenC5{a5,1}{1,2}, a5, LstCenC5{a5,1}{1,3}, '*'), hold on
grid on
alpha(0);
end
 
%plotting triangles and centroids for Concept 6
figure
for a6=1:50
point1c6=[final{a6,1}{1,6}(1,1) a6 0];
point2c6=[final{a6,1}{1,6}(1,2) a6 1];
point3c6=[final{a6,1}{1,6}(1,3) a6 0];
pointsc6=[point1c6' point2c6' point3c6'];
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]); 
fill3(pointsc6(1,:),pointsc6(2,:),pointsc6(3,:),'b'), hold on
scatter3(LstCenC6{a6,1}{1,2}, a6, LstCenC6{a6,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 7
figure
for a7=1:50
point1c7=[final{a7,1}{1,7}(1,1) a7 0];
point2c7=[final{a7,1}{1,7}(1,2) a7 1];
point3c7=[final{a7,1}{1,7}(1,3) a7 0];
pointsc7=[point1c7' point2c7' point3c7'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc7(1,:),pointsc7(2,:),pointsc7(3,:),'b'), hold on
scatter3(LstCenC7{a7,1}{1,2}, a7, LstCenC7{a7,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 8
figure
for a8=1:50
point1c8=[final{a8,1}{1,8}(1,1) a8 0];
point2c8=[final{a8,1}{1,8}(1,2) a8 1];
point3c8=[final{a8,1}{1,8}(1,3) a8 0];
pointsc8=[point1c8' point2c8' point3c8'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc8(1,:),pointsc8(2,:),pointsc8(3,:),'b'), hold on
scatter3(LstCenC8{a8,1}{1,2}, a8, LstCenC8{a8,1}{1,3}, '*'), hold on
grid on
alpha(0);
end


%plotting triangles and centroids for Concept 9
figure
for a9=1:50
point1c9=[final{a9,1}{1,9}(1,1) a9 0];
point2c9=[final{a9,1}{1,9}(1,2) a9 1];
point3c9=[final{a9,1}{1,9}(1,3) a9 0];
pointsc9=[point1c9' point2c9' point3c9'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc9(1,:),pointsc9(2,:),pointsc9(3,:),'b'), hold on
scatter3(LstCenC9{a9,1}{1,2}, a9, LstCenC9{a9,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 10

figure
for a10=1:50
point1c10=[final{a10,1}{1,10}(1,1) a10 0];
point2c10=[final{a10,1}{1,10}(1,2) a10 1];
point3c10=[final{a10,1}{1,10}(1,3) a10 0];
pointsc10=[point1c10' point2c10' point3c10'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc10(1,:),pointsc10(2,:),pointsc10(3,:),'b'), hold on
scatter3(LstCenC10{a10,1}{1,2}, a10, LstCenC10{a10,1}{1,3}, '*'), hold on
grid on
alpha(0);
end


%plotting triangles and centroids for Concept 11
figure
for a11=1:50
point1c11=[final{a11,1}{1,11}(1,1) a11 0];
point2c11=[final{a11,1}{1,11}(1,2) a11 1];
point3c11=[final{a11,1}{1,11}(1,3) a11 0];
pointsc11=[point1c11' point2c11' point3c11'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc11(1,:),pointsc11(2,:),pointsc11(3,:),'b'), hold on
scatter3(LstCenC11{a11,1}{1,2}, a11, LstCenC11{a11,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 12
figure
for a12=1:50
point1c12=[final{a12,1}{1,12}(1,1) a12 0];
point2c12=[final{a12,1}{1,12}(1,2) a12 1];
point3c12=[final{a12,1}{1,12}(1,3) a12 0];
pointsc12=[point1c12' point2c12' point3c12'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc12(1,:),pointsc12(2,:),pointsc12(3,:),'b'), hold on
scatter3(LstCenC12{a12,1}{1,2}, a12, LstCenC12{a12,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 13

figure
for a13=1:50
point1c13=[final{a13,1}{1,13}(1,1) a13 0];
point2c13=[final{a13,1}{1,13}(1,2) a13 1];
point3c13=[final{a13,1}{1,13}(1,3) a13 0];
pointsc13=[point1c13' point2c13' point3c13'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc13(1,:),pointsc13(2,:),pointsc13(3,:),'b'), hold on
scatter3(LstCenC13{a13,1}{1,2}, a13, LstCenC13{a13,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 14
figure
for a14=1:50
point1c14=[final{a14,1}{1,14}(1,1) a14 0];
point2c14=[final{a14,1}{1,14}(1,2) a14 1];
point3c14=[final{a14,1}{1,14}(1,3) a14 0];
pointsc14=[point1c14' point2c14' point3c14'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc14(1,:),pointsc14(2,:),pointsc14(3,:),'b'), hold on
scatter3(LstCenC14{a14,1}{1,2}, a14, LstCenC14{a14,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 15
figure
for a15=1:50
point1c15=[final{a15,1}{1,15}(1,1) a15 0];
point2c15=[final{a15,1}{1,15}(1,2) a15 1];
point3c15=[final{a15,1}{1,15}(1,3) a15 0];
pointsc15=[point1c15' point2c15' point3c15'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc15(1,:),pointsc15(2,:),pointsc15(3,:),'b'), hold on
scatter3(LstCenC15{a15,1}{1,2}, a15, LstCenC15{a15,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 16
figure
for a16=1:50
point1c16=[final{a16,1}{1,16}(1,1) a16 0];
point2c16=[final{a16,1}{1,16}(1,2) a16 1];
point3c16=[final{a16,1}{1,16}(1,3) a16 0];
pointsc16=[point1c16' point2c16' point3c16'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc16(1,:),pointsc16(2,:),pointsc16(3,:),'b'), hold on
scatter3(LstCenC16{a16,1}{1,2}, a16, LstCenC16{a16,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 17
figure
for a17=1:50
point1c17=[final{a17,1}{1,17}(1,1) a17 0];
point2c17=[final{a17,1}{1,17}(1,2) a17 1];
point3c17=[final{a17,1}{1,17}(1,3) a17 0];
pointsc17=[point1c17' point2c17' point3c17'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc17(1,:),pointsc17(2,:),pointsc17(3,:),'b'), hold on
scatter3(LstCenC17{a17,1}{1,2}, a17, LstCenC17{a17,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 18
figure
for a18=1:50
point1c18=[final{a18,1}{1,18}(1,1) a18 0];
point2c18=[final{a18,1}{1,18}(1,2) a18 1];
point3c18=[final{a18,1}{1,18}(1,3) a18 0];
pointsc18=[point1c18' point2c18' point3c18'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc18(1,:),pointsc18(2,:),pointsc18(3,:),'b'), hold on
scatter3(LstCenC18{a18,1}{1,2}, a18, LstCenC18{a18,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

%plotting triangles and centroids for Concept 19

figure
for a19=1:50
point1c19=[final{a19,1}{1,19}(1,1) a19 0];
point2c19=[final{a19,1}{1,19}(1,2) a19 1];
point3c19=[final{a19,1}{1,19}(1,3) a19 0];
pointsc19=[point1c19' point2c19' point3c19'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc19(1,:),pointsc19(2,:),pointsc19(3,:),'b'), hold on
scatter3(LstCenC19{a19,1}{1,2}, a19, LstCenC19{a19,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a20=1:50
point1c20=[final{a20,1}{1,20}(1,1) a20 0];
point2c20=[final{a20,1}{1,20}(1,2) a20 1];
point3c20=[final{a20,1}{1,20}(1,3) a20 0];
pointsc20=[point1c20' point2c20' point3c20'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc20(1,:),pointsc20(2,:),pointsc20(3,:),'b'), hold on
scatter3(LstCenC20{a20,1}{1,2}, a20, LstCenC20{a20,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a21=1:50
point1c21=[final{a21,1}{1,21}(1,1) a21 0];
point2c21=[final{a21,1}{1,21}(1,2) a21 1];
point3c21=[final{a21,1}{1,21}(1,3) a21 0];
pointsc21=[point1c21' point2c21' point3c21'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc21(1,:),pointsc21(2,:),pointsc21(3,:),'b'), hold on
scatter3(LstCenC21{a21,1}{1,2}, a21, LstCenC21{a21,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a22=1:50
point1c22=[final{a22,1}{1,22}(1,1) a22 0];
point2c22=[final{a22,1}{1,22}(1,2) a22 1];
point3c22=[final{a22,1}{1,22}(1,3) a22 0];
pointsc22=[point1c22' point2c22' point3c22'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc22(1,:),pointsc22(2,:),pointsc22(3,:),'b'), hold on
scatter3(LstCenC22{a22,1}{1,2}, a22, LstCenC22{a22,1}{1,3}, '*'), hold on
grid on
alpha(0);
end
%{
figure
for a23=2:50
point1c23=[final{a23,1}{1,22}(1,1) a23 0];
point2c23=[final{a23,1}{1,22}(1,2) a23 1];
point3c23=[final{a23,1}{1,22}(1,3) a23 0];
pointsc23=[point1c23' point2c23' point3c23'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc23(1,:),pointsc23(2,:),pointsc23(3,:),'b'), hold on
scatter3(LstCenC23{a23,1}{1,2}, a23, LstCenC23{a23,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a24=2:50
point1c24=[final{a24,1}{1,22}(1,1) a24 0];
point2c24=[final{a24,1}{1,22}(1,2) a24 1];
point3c24=[final{a24,1}{1,22}(1,3) a24 0];
pointsc24=[point1c24' point2c24' point3c24'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc24(1,:),pointsc24(2,:),pointsc24(3,:),'b'), hold on
scatter3(LstCenC24{a24,1}{1,2}, a24, LstCenC24{a24,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a25=2:50
point1c25=[final{a25,1}{1,22}(1,1) a25 0];
point2c25=[final{a25,1}{1,22}(1,2) a25 1];
point3c25=[final{a25,1}{1,22}(1,3) a25 0];
pointsc25=[point1c25' point2c25' point3c25'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc25(1,:),pointsc25(2,:),pointsc25(3,:),'b'), hold on
scatter3(LstCenC25{a25,1}{1,2}, a25, LstCenC25{a25,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a26=2:50
point1c26=[final{a26,1}{1,22}(1,1) a26 0];
point2c26=[final{a26,1}{1,22}(1,2) a26 1];
point3c26=[final{a26,1}{1,22}(1,3) a26 0];
pointsc26=[point1c26' point2c26' point3c26'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc26(1,:),pointsc26(2,:),pointsc26(3,:),'b'), hold on
scatter3(LstCenC26{a26,1}{1,2}, a26, LstCenC26{a26,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a27=2:50
point1c27=[final{a27,1}{1,22}(1,1) a27 0];
point2c27=[final{a27,1}{1,22}(1,2) a27 1];
point3c27=[final{a27,1}{1,22}(1,3) a27 0];
pointsc27=[point1c27' point2c27' point3c27'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc27(1,:),pointsc27(2,:),pointsc27(3,:),'b'), hold on
scatter3(LstCenC27{a27,1}{1,2}, a27, LstCenC27{a27,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a28=2:50
point1c28=[final{a28,1}{1,22}(1,1) a28 0];
point2c28=[final{a28,1}{1,22}(1,2) a28 1];
point3c28=[final{a28,1}{1,22}(1,3) a28 0];
pointsc28=[point1c28' point2c28' point3c28'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc28(1,:),pointsc28(2,:),pointsc28(3,:),'b'), hold on
scatter3(LstCenC28{a28,1}{1,2}, a28, LstCenC28{a28,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a29=2:50
point1c29=[final{a29,1}{1,22}(1,1) a29 0];
point2c29=[final{a29,1}{1,22}(1,2) a29 1];
point3c29=[final{a29,1}{1,22}(1,3) a29 0];
pointsc29=[point1c29' point2c29' point3c29'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc29(1,:),pointsc29(2,:),pointsc29(3,:),'b'), hold on
scatter3(LstCenC29{a29,1}{1,2}, a29, LstCenC29{a29,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a30=2:50
point1c30=[final{a30,1}{1,22}(1,1) a30 0];
point2c30=[final{a30,1}{1,22}(1,2) a30 1];
point3c30=[final{a30,1}{1,22}(1,3) a30 0];
pointsc30=[point1c30' point2c30' point3c30'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc30(1,:),pointsc30(2,:),pointsc30(3,:),'b'), hold on
scatter3(LstCenC30{a30,1}{1,2}, a30, LstCenC30{a30,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

for a31=2:50
point1c31=[final{a31,1}{1,22}(1,1) a31 0];
point2c31=[final{a31,1}{1,22}(1,2) a31 1];
point3c31=[final{a31,1}{1,22}(1,3) a31 0];
pointsc31=[point1c31' point2c31' point3c31'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc31(1,:),pointsc31(2,:),pointsc31(3,:),'b'), hold on
scatter3(LstCenC31{a31,1}{1,2}, a31, LstCenC31{a31,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a32=3:50
point1c32=[final{a32,1}{1,32}(1,1) a32 0];
point2c32=[final{a32,1}{1,32}(1,2) a32 1];
point3c32=[final{a32,1}{1,32}(1,3) a32 0];
pointsc32=[point1c32' point2c32' point3c32'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc32(1,:),pointsc32(2,:),pointsc32(3,:),'b'), hold on
scatter3(LstCenC32{a32,1}{1,2}, a32, LstCenC32{a32,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a33=2:50
point1c33=[final{a33,1}{1,22}(1,1) a33 0];
point2c33=[final{a33,1}{1,22}(1,2) a33 1];
point3c33=[final{a33,1}{1,22}(1,3) a33 0];
pointsc33=[point1c33' point2c33' point3c33'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc33(1,:),pointsc33(2,:),pointsc33(3,:),'b'), hold on
scatter3(LstCenC33{a33,1}{1,2}, a33, LstCenC33{a33,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a34=2:50
point1c34=[final{a34,1}{1,22}(1,1) a34 0];
point2c34=[final{a34,1}{1,22}(1,2) a34 1];
point3c34=[final{a34,1}{1,22}(1,3) a34 0];
pointsc34=[point1c34' point2c34' point3c34'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc34(1,:),pointsc34(2,:),pointsc34(3,:),'b'), hold on
scatter3(LstCenC34{a34,1}{1,2}, a34, LstCenC34{a34,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a35=2:50
point1c35=[final{a35,1}{1,22}(1,1) a35 0];
point2c35=[final{a35,1}{1,22}(1,2) a35 1];
point3c35=[final{a35,1}{1,22}(1,3) a35 0];
pointsc35=[point1c35' point2c35' point3c35'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc35(1,:),pointsc35(2,:),pointsc35(3,:),'b'), hold on
scatter3(LstCenC35{a35,1}{1,2}, a35, LstCenC35{a35,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a36=2:50
point1c36=[final{a36,1}{1,22}(1,1) a36 0];
point2c36=[final{a36,1}{1,22}(1,2) a36 1];
point3c36=[final{a36,1}{1,22}(1,3) a36 0];
pointsc36=[point1c36' point2c36' point3c36'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc36(1,:),pointsc36(2,:),pointsc36(3,:),'b'), hold on
scatter3(LstCenC36{a36,1}{1,2}, a36, LstCenC36{a36,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a37=2:50
point1c37=[final{a37,1}{1,22}(1,1) a37 0];
point2c37=[final{a37,1}{1,22}(1,2) a37 1];
point3c37=[final{a37,1}{1,22}(1,3) a37 0];
pointsc37=[point1c37' point2c37' point3c37'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc37(1,:),pointsc37(2,:),pointsc37(3,:),'b'), hold on
scatter3(LstCenC37{a37,1}{1,2}, a37, LstCenC37{a37,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a38=2:50
point1c38=[final{a38,1}{1,22}(1,1) a38 0];
point2c38=[final{a38,1}{1,22}(1,2) a38 1];
point3c38=[final{a38,1}{1,22}(1,3) a38 0];
pointsc38=[point1c38' point2c38' point3c38'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc38(1,:),pointsc38(2,:),pointsc38(3,:),'b'), hold on
scatter3(LstCenC38{a38,1}{1,2}, a38, LstCenC38{a38,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a39=2:50
point1c39=[final{a39,1}{1,22}(1,1) a39 0];
point2c39=[final{a39,1}{1,22}(1,2) a39 1];
point3c39=[final{a39,1}{1,22}(1,3) a39 0];
pointsc39=[point1c39' point2c39' point3c39'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc39(1,:),pointsc39(2,:),pointsc39(3,:),'b'), hold on
scatter3(LstCenC39{a39,1}{1,2}, a39, LstCenC39{a39,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a40=2:50
point1c40=[final{a40,1}{1,22}(1,1) a40 0];
point2c40=[final{a40,1}{1,22}(1,2) a40 1];
point3c40=[final{a40,1}{1,22}(1,3) a40 0];
pointsc40=[point1c40' point2c40' point3c40'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc40(1,:),pointsc40(2,:),pointsc40(3,:),'b'), hold on
scatter3(LstCenC40{a40,1}{1,2}, a40, LstCenC40{a40,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a41=2:50
point1c41=[final{a41,1}{1,22}(1,1) a41 0];
point2c41=[final{a41,1}{1,22}(1,2) a41 1];
point3c41=[final{a41,1}{1,22}(1,3) a41 0];
pointsc41=[point1c41' point2c41' point3c41'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc41(1,:),pointsc41(2,:),pointsc41(3,:),'b'), hold on
scatter3(LstCenC41{a41,1}{1,2}, a41, LstCenC41{a41,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a42=2:50
point1c42=[final{a42,1}{1,22}(1,1) a42 0];
point2c42=[final{a42,1}{1,22}(1,2) a42 1];
point3c42=[final{a42,1}{1,22}(1,3) a42 0];
pointsc42=[point1c42' point2c42' point3c42'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc42(1,:),pointsc42(2,:),pointsc42(3,:),'b'), hold on
scatter3(LstCenC42{a42,1}{1,2}, a42, LstCenC42{a42,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a43=2:50
point1c43=[final{a43,1}{1,22}(1,1) a43 0];
point2c43=[final{a43,1}{1,22}(1,2) a43 1];
point3c43=[final{a43,1}{1,22}(1,3) a43 0];
pointsc43=[point1c43' point2c43' point3c43'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc43(1,:),pointsc43(2,:),pointsc43(3,:),'b'), hold on
scatter3(LstCenC43{a43,1}{1,2}, a43, LstCenC43{a43,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a44=2:50
point1c44=[final{a44,1}{1,22}(1,1) a44 0];
point2c44=[final{a44,1}{1,22}(1,2) a44 1];
point3c44=[final{a44,1}{1,22}(1,3) a44 0];
pointsc44=[point1c44' point2c44' point3c44'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc44(1,:),pointsc44(2,:),pointsc44(3,:),'b'), hold on
scatter3(LstCenC44{a44,1}{1,2}, a44, LstCenC44{a44,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a45=2:50
point1c45=[final{a45,1}{1,22}(1,1) a45 0];
point2c45=[final{a45,1}{1,22}(1,2) a45 1];
point3c45=[final{a45,1}{1,22}(1,3) a45 0];
pointsc45=[point1c45' point2c45' point3c45'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc45(1,:),pointsc45(2,:),pointsc45(3,:),'b'), hold on
scatter3(LstCenC45{a45,1}{1,2}, a45, LstCenC45{a45,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a46=2:50
point1c46=[final{a46,1}{1,22}(1,1) a46 0];
point2c46=[final{a46,1}{1,22}(1,2) a46 1];
point3c46=[final{a46,1}{1,22}(1,3) a46 0];
pointsc46=[point1c46' point2c46' point3c46'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc46(1,:),pointsc46(2,:),pointsc46(3,:),'b'), hold on
scatter3(LstCenC46{a46,1}{1,2}, a46, LstCenC46{a46,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a47=2:50
point1c47=[final{a47,1}{1,22}(1,1) a47 0];
point2c47=[final{a47,1}{1,22}(1,2) a47 1];
point3c47=[final{a47,1}{1,22}(1,3) a47 0];
pointsc47=[point1c47' point2c47' point3c47'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc47(1,:),pointsc47(2,:),pointsc47(3,:),'b'), hold on
scatter3(LstCenC47{a47,1}{1,2}, a47, LstCenC47{a47,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a48=2:50
point1c48=[final{a48,1}{1,22}(1,1) a48 0];
point2c48=[final{a48,1}{1,22}(1,2) a48 1];
point3c48=[final{a48,1}{1,22}(1,3) a48 0];
pointsc48=[point1c48' point2c48' point3c48'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc48(1,:),pointsc48(2,:),pointsc48(3,:),'b'), hold on
scatter3(LstCenC48{a48,1}{1,2}, a48, LstCenC48{a48,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a49=2:50
point1c49=[final{a49,1}{1,22}(1,1) a49 0];
point2c49=[final{a49,1}{1,22}(1,2) a49 1];
point3c49=[final{a49,1}{1,22}(1,3) a49 0];
pointsc49=[point1c49' point2c49' point3c49'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc49(1,:),pointsc49(2,:),pointsc49(3,:),'b'), hold on
scatter3(LstCenC49{a49,1}{1,2}, a49, LstCenC49{a49,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a50=2:50
point1c50=[final{a50,1}{1,22}(1,1) a50 0];
point2c50=[final{a50,1}{1,22}(1,2) a50 1];
point3c50=[final{a50,1}{1,22}(1,3) a50 0];
pointsc50=[point1c50' point2c50' point3c50'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc50(1,:),pointsc50(2,:),pointsc50(3,:),'b'), hold on
scatter3(LstCenC50{a50,1}{1,2}, a50, LstCenC50{a50,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a51=2:50
point1c51=[final{a51,1}{1,22}(1,1) a51 0];
point2c51=[final{a51,1}{1,22}(1,2) a51 1];
point3c51=[final{a51,1}{1,22}(1,3) a51 0];
pointsc51=[point1c51' point2c51' point3c51'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc51(1,:),pointsc51(2,:),pointsc51(3,:),'b'), hold on
scatter3(LstCenC51{a51,1}{1,2}, a51, LstCenC51{a51,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a52=2:50
point1c52=[final{a52,1}{1,22}(1,1) a52 0];
point2c52=[final{a52,1}{1,22}(1,2) a52 1];
point3c52=[final{a52,1}{1,22}(1,3) a52 0];
pointsc52=[point1c52' point2c52' point3c52'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc52(1,:),pointsc52(2,:),pointsc52(3,:),'b'), hold on
scatter3(LstCenC52{a52,1}{1,2}, a52, LstCenC52{a52,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a53=2:50
point1c53=[final{a53,1}{1,22}(1,1) a53 0];
point2c53=[final{a53,1}{1,22}(1,2) a53 1];
point3c53=[final{a53,1}{1,22}(1,3) a53 0];
pointsc53=[point1c53' point2c53' point3c53'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc53(1,:),pointsc53(2,:),pointsc53(3,:),'b'), hold on
scatter3(LstCenC53{a53,1}{1,2}, a53, LstCenC53{a53,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a54=2:50
point1c54=[final{a54,1}{1,22}(1,1) a54 0];
point2c54=[final{a54,1}{1,22}(1,2) a54 1];
point3c54=[final{a54,1}{1,22}(1,3) a54 0];
pointsc54=[point1c54' point2c54' point3c54'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc54(1,:),pointsc54(2,:),pointsc54(3,:),'b'), hold on
scatter3(LstCenC54{a54,1}{1,2}, a54, LstCenC54{a54,1}{1,3}, '*'), hold on
grid on
alpha(0);
end


figure
for a55=2:50
point1c55=[final{a55,1}{1,22}(1,1) a55 0];
point2c55=[final{a55,1}{1,22}(1,2) a55 1];
point3c55=[final{a55,1}{1,22}(1,3) a55 0];
pointsc55=[point1c55' point2c55' point3c55'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc55(1,:),pointsc55(2,:),pointsc55(3,:),'b'), hold on
scatter3(LstCenC55{a55,1}{1,2}, a55, LstCenC55{a55,1}{1,3}, '*'), hold on
grid on
alpha(0);
end


figure
for a56=2:50
point1c56=[final{a56,1}{1,22}(1,1) a56 0];
point2c56=[final{a56,1}{1,22}(1,2) a56 1];
point3c56=[final{a56,1}{1,22}(1,3) a56 0];
pointsc56=[point1c56' point2c56' point3c56'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc56(1,:),pointsc56(2,:),pointsc56(3,:),'b'), hold on
scatter3(LstCenC56{a56,1}{1,2}, a56, LstCenC56{a56,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a57=2:50
point1c57=[final{a57,1}{1,22}(1,1) a57 0];
point2c57=[final{a57,1}{1,22}(1,2) a57 1];
point3c57=[final{a57,1}{1,22}(1,3) a57 0];
pointsc57=[point1c57' point2c57' point3c57'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc57(1,:),pointsc57(2,:),pointsc57(3,:),'b'), hold on
scatter3(LstCenC57{a57,1}{1,2}, a57, LstCenC57{a57,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a58=2:50
point1c58=[final{a58,1}{1,22}(1,1) a58 0];
point2c58=[final{a58,1}{1,22}(1,2) a58 1];
point3c58=[final{a58,1}{1,22}(1,3) a58 0];
pointsc58=[point1c58' point2c58' point3c58'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc58(1,:),pointsc58(2,:),pointsc58(3,:),'b'), hold on
scatter3(LstCenC58{a58,1}{1,2}, a58, LstCenC58{a58,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a59=2:50
point1c59=[final{a59,1}{1,22}(1,1) a59 0];
point2c59=[final{a59,1}{1,22}(1,2) a59 1];
point3c59=[final{a59,1}{1,22}(1,3) a59 0];
pointsc59=[point1c59' point2c59' point3c59'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc59(1,:),pointsc59(2,:),pointsc59(3,:),'b'), hold on
scatter3(LstCenC59{a59,1}{1,2}, a59, LstCenC59{a59,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a60=2:50
point1c60=[final{a60,1}{1,22}(1,1) a60 0];
point2c60=[final{a60,1}{1,22}(1,2) a60 1];
point3c60=[final{a60,1}{1,22}(1,3) a60 0];
pointsc60=[point1c60' point2c60' point3c60'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc60(1,:),pointsc60(2,:),pointsc60(3,:),'b'), hold on
scatter3(LstCenC60{a60,1}{1,2}, a60, LstCenC60{a60,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a61=2:50
point1c61=[final{a61,1}{1,22}(1,1) a61 0];
point2c61=[final{a61,1}{1,22}(1,2) a61 1];
point3c61=[final{a61,1}{1,22}(1,3) a61 0];
pointsc61=[point1c61' point2c61' point3c61'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc61(1,:),pointsc61(2,:),pointsc61(3,:),'b'), hold on
scatter3(LstCenC61{a61,1}{1,2}, a61, LstCenC61{a61,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure

for a62=2:50
point1c62=[final{a62,1}{1,22}(1,1) a62 0];
point2c62=[final{a62,1}{1,22}(1,2) a62 1];
point3c62=[final{a62,1}{1,22}(1,3) a62 0];
pointsc62=[point1c62' point2c62' point3c62'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc62(1,:),pointsc62(2,:),pointsc62(3,:),'b'), hold on
scatter3(LstCenC62{a62,1}{1,2}, a62, LstCenC62{a62,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a63=2:50
point1c63=[final{a63,1}{1,22}(1,1) a63 0];
point2c63=[final{a63,1}{1,22}(1,2) a63 1];
point3c63=[final{a63,1}{1,22}(1,3) a63 0];
pointsc63=[point1c63' point2c63' point3c63'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc63(1,:),pointsc63(2,:),pointsc63(3,:),'b'), hold on
scatter3(LstCenC63{a63,1}{1,2}, a63, LstCenC63{a63,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a64=2:50
point1c64=[final{a64,1}{1,22}(1,1) a64 0];
point2c64=[final{a64,1}{1,22}(1,2) a64 1];
point3c64=[final{a64,1}{1,22}(1,3) a64 0];
pointsc64=[point1c64' point2c64' point3c64'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc64(1,:),pointsc64(2,:),pointsc64(3,:),'b'), hold on
scatter3(LstCenC64{a64,1}{1,2}, a64, LstCenC64{a64,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a65=2:50
point1c65=[final{a65,1}{1,22}(1,1) a65 0];
point2c65=[final{a65,1}{1,22}(1,2) a65 1];
point3c65=[final{a65,1}{1,22}(1,3) a65 0];
pointsc65=[point1c65' point2c65' point3c65'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc65(1,:),pointsc65(2,:),pointsc65(3,:),'b'), hold on
scatter3(LstCenC65{a65,1}{1,2}, a65, LstCenC65{a65,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a66=2:50
point1c66=[final{a66,1}{1,66}(1,1) a66 0];
point2c66=[final{a66,1}{1,66}(1,2) a66 1];
point3c66=[final{a66,1}{1,66}(1,3) a66 0];
pointsc66=[point1c66' point2c66' point3c66'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc66(1,:),pointsc66(2,:),pointsc66(3,:),'b'), hold on
scatter3(LstCenC66{a66,1}{1,2}, a66, LstCenC66{a66,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a67=2:50
point1c67=[final{a67,1}{1,22}(1,1) a67 0];
point2c67=[final{a67,1}{1,22}(1,2) a67 1];
point3c67=[final{a67,1}{1,22}(1,3) a67 0];
pointsc67=[point1c67' point2c67' point3c67'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc67(1,:),pointsc67(2,:),pointsc67(3,:),'b'), hold on
scatter3(LstCenC67{a67,1}{1,2}, a67, LstCenC67{a67,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a68=2:50
point1c68=[final{a68,1}{1,22}(1,1) a68 0];
point2c68=[final{a68,1}{1,22}(1,2) a68 1];
point3c68=[final{a68,1}{1,22}(1,3) a68 0];
pointsc68=[point1c68' point2c68' point3c68'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc68(1,:),pointsc68(2,:),pointsc68(3,:),'b'), hold on
scatter3(LstCenC68{a68,1}{1,2}, a68, LstCenC68{a68,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a69=2:50
point1c69=[final{a69,1}{1,22}(1,1) a69 0];
point2c69=[final{a69,1}{1,22}(1,2) a69 1];
point3c69=[final{a69,1}{1,22}(1,3) a69 0];
pointsc69=[point1c69' point2c69' point3c69'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc69(1,:),pointsc69(2,:),pointsc69(3,:),'b'), hold on
scatter3(LstCenC69{a69,1}{1,2}, a69, LstCenC69{a69,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a70=2:50
point1c70=[final{a70,1}{1,22}(1,1) a70 0];
point2c70=[final{a70,1}{1,22}(1,2) a70 1];
point3c70=[final{a70,1}{1,22}(1,3) a70 0];
pointsc70=[point1c70' point2c70' point3c70'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc70(1,:),pointsc70(2,:),pointsc70(3,:),'b'), hold on
scatter3(LstCenC70{a70,1}{1,2}, a70, LstCenC70{a70,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a71=2:50
point1c71=[final{a71,1}{1,22}(1,1) a71 0];
point2c71=[final{a71,1}{1,22}(1,2) a71 1];
point3c71=[final{a71,1}{1,22}(1,3) a71 0];
pointsc71=[point1c71' point2c71' point3c71'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc71(1,:),pointsc71(2,:),pointsc71(3,:),'b'), hold on
scatter3(LstCenC71{a71,1}{1,2}, a71, LstCenC71{a71,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a72=2:50
point1c72=[final{a72,1}{1,22}(1,1) a72 0];
point2c72=[final{a72,1}{1,22}(1,2) a72 1];
point3c72=[final{a72,1}{1,22}(1,3) a72 0];
pointsc72=[point1c72' point2c72' point3c72'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc72(1,:),pointsc72(2,:),pointsc72(3,:),'b'), hold on
scatter3(LstCenC72{a72,1}{1,2}, a72, LstCenC72{a72,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a73=2:50
point1c73=[final{a73,1}{1,22}(1,1) a73 0];
point2c73=[final{a73,1}{1,22}(1,2) a73 1];
point3c73=[final{a73,1}{1,22}(1,3) a73 0];
pointsc73=[point1c73' point2c73' point3c73'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc73(1,:),pointsc73(2,:),pointsc73(3,:),'b'), hold on
scatter3(LstCenC73{a73,1}{1,2}, a73, LstCenC73{a73,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a74=2:50
point1c74=[final{a74,1}{1,22}(1,1) a74 0];
point2c74=[final{a74,1}{1,22}(1,2) a74 1];
point3c74=[final{a74,1}{1,22}(1,3) a74 0];
pointsc74=[point1c74' point2c74' point3c74'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc74(1,:),pointsc74(2,:),pointsc74(3,:),'b'), hold on
scatter3(LstCenC74{a74,1}{1,2}, a74, LstCenC74{a74,1}{1,3}, '*'), hold on
grid on
alpha(0);
end

figure
for a75=2:50
point1c75=[final{a75,1}{1,22}(1,1) a75 0];
point2c75=[final{a75,1}{1,22}(1,2) a75 1];
point3c75=[final{a75,1}{1,22}(1,3) a75 0];
pointsc75=[point1c75' point2c75' point3c75'];
 
xlim([-1 1]); 
ylim([1 50]);
zlim([0 1]);  
fill3(pointsc75(1,:),pointsc75(2,:),pointsc75(3,:),'b'), hold on
scatter3(LstCenC75{a75,1}{1,2}, a75, LstCenC75{a75,1}{1,3}, '*'), hold on
grid on
alpha(0);
end;
%}
